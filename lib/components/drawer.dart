import 'package:flutter/material.dart';
import 'package:words/pages/game_page.dart';
import 'package:words/pages/login_page.dart';
import 'package:words/pages/users_page.dart';
import 'package:words/services/shared_preferences_manager.dart';

class DrawerWords extends StatelessWidget {
  const DrawerWords({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          ListTile(
            leading: Icon(Icons.videogame_asset_outlined),
            title: Text('Play'),
            onTap: () async {
              await SharedPreferencesManager().clearData();

              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (_) => GamePage()),
                      (Route<dynamic> route) => false
              );
            },
          ),
          ListTile(
            leading: Icon(Icons.person),
            title: Text('Users'),
            onTap: () async {

              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (_) => UsersPage()),
                      (Route<dynamic> route) => false
              );
            },
          ),
          const Padding(
            padding: EdgeInsets.only(left: 10, right: 10),
            child: Divider(color: Colors.black87),
          ),
          ListTile(
            leading: Icon(Icons.logout),
            title: Text('Log out'),
            onTap: () async {
              await SharedPreferencesManager().clearData();

              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (_) => LoginPage()),
                      (Route<dynamic> route) => false
              );
            },
          )
        ],
      ),
    );
  }
}
