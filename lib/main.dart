import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:words/pages/game_page.dart';
import 'package:words/pages/login_page.dart';
import 'package:words/providers/match_provider.dart';
import 'package:words/services/shared_preferences_manager.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  var hasToken = false;
  @override
  void initState() {
    checkLoggedIn();
    super.initState();
  }

  void checkLoggedIn() async {
    var token = await SharedPreferencesManager().getToken();

    setState(() {
      hasToken = token != null ? true : false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => MatchProvider())
      ],
      child: MaterialApp(
        home: hasToken ? GamePage() : LoginPage()
      ),
    );
  }
}

