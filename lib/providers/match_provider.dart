import 'package:flutter/material.dart';
import 'package:words/models/letter.dart';

class MatchProvider extends ChangeNotifier{

  List<Letter> _letters = [];
  List<Letter> _result = [];
  bool _loadingLetters = false;

  get loadingLetters{
    return _loadingLetters;
  }

  set loadingLetters(value){
    _loadingLetters = value;
    notifyListeners();
  }

  get letters{
    return _letters;
  }

  get result{
    return _result;
  }

  setLetters(List<String> letters){
    _letters = [];
    _result = [];

    for(var i = 0; i < letters.length; i++){
      Letter letter = Letter(letter: letters[i], position: i);
      _letters.add(letter);
    }
    /*
    for(String element in letters){
      Letter letter = Letter(letter: element, position: letters.indexOf(element));
      _letters.add(letter);
    }*/
    notifyListeners();
  }

  addLetter(Letter letter){
    _letters[letter.position].used = true;
    _result.add(letter);
    notifyListeners();
  }

  removeLetter(Letter letter){
    _result.remove(letter);
    _letters[letter.position].used = false;
    notifyListeners();
  }

  removeLastLetter(){
    var letter = _result.removeLast();
    _letters[letter.position].used = false;
    notifyListeners();
  }
}