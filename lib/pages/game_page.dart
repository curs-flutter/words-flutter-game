import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:words/components/drawer.dart';
import 'package:words/models/letter.dart';
import 'package:words/providers/match_provider.dart';
import 'package:words/repositories/match_repository.dart';

class GamePage extends StatefulWidget {
  const GamePage({Key? key}) : super(key: key);

  @override
  State<GamePage> createState() => _GamePageState();
}

class _GamePageState extends State<GamePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: DrawerWords(),
      appBar: AppBar(
        title: const Text('Game page'),
        actions: [
          Consumer<MatchProvider>(
            builder: (context, matchProvider, _) {
              if (matchProvider.loadingLetters) {
                return CircularProgressIndicator();
              } else {
                return IconButton(
                    onPressed: () async {
                      var matchProvider = Provider.of<MatchProvider>(context, listen: false);

                      matchProvider.loadingLetters = true;

                      try {
                        var match = await MatchRepository().postCreateMatch();
                        var matchStarted = await MatchRepository().startMatch(match.id);
                        matchProvider.setLetters(matchStarted.letters);

                        print('Match id: ${matchStarted.id}');
                        print('Match Letters: ${matchStarted.letters}');
                        print('Match Words: ${matchStarted.words}');
                      } catch (err) {
                        print('Error when start match: $err');
                      }

                      matchProvider.loadingLetters = false;
                    },
                    icon: Icon(Icons.add)
                );
              }
            }
          )
        ],
      ),
      body: Column(
        children: [
          Flexible(
            flex: 1,
            child: Column(
              children: [
                Flexible(
                  flex: 1,
                  child: Container(
                    alignment: Alignment.topRight,
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    color: Colors.blue,
                    child: Text('Clock')
                  )
                ),
                Flexible(
                  flex: 2,
                  child: ResultBox()
                ),
              ],
            ),
          ),
          Flexible(
            child: LettersBox(),
            flex: 1,
          )
        ],
      ),
    );
  }
}

class ResultBox extends StatelessWidget {
  const ResultBox({
    Key? key,

  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    var result = Provider.of<MatchProvider>(context).result;

    return Container(
      color: Colors.green,
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: result.length > 0 ? GridView.builder(
        padding: EdgeInsets.only(top: 5, right: 5, left: 5),
        shrinkWrap: true,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 8,
          crossAxisSpacing: 4.0,
          mainAxisSpacing: 4.0
        ),
        itemCount: result.length,
        itemBuilder: (BuildContext context, int index){
          return LetterCard(letter: result[index], color: Colors.white54, fontSize: 40);
        }
      ) : Text('Press + to start match'),
    );
  }
}

class LettersBox extends StatelessWidget {
  const LettersBox({
    Key? key,

  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    var letters = Provider.of<MatchProvider>(context).letters;

    return Container(
      color: Colors.white,
      child: letters.length > 0 ? GridView.builder(
        padding: EdgeInsets.only(top: 5, right: 5, left: 5),
        shrinkWrap: true,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 6,
          crossAxisSpacing: 4.0,
          mainAxisSpacing: 4.0
        ),
        itemCount: letters.length,
        itemBuilder: (BuildContext context, int index){

          if(!letters[index].used){
            return LetterCard(letter: letters[index], color: Colors.blueGrey, fontSize: 50);
          }else{
            return LetterCard(color: Colors.red, fontSize: 50);
          }

        }
      ) : Text('Press + to start match'),
    );
  }
}

class LetterCard extends StatelessWidget {
  final Letter? letter;
  final Color color;
  final double fontSize;

  const LetterCard({
    Key? key,
    this.letter,
    required this.color,
    required this.fontSize
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        if(letter != null){
          if(letter!.used){
            Provider.of<MatchProvider>(context, listen: false).removeLetter(letter!);
          }else{
            Provider.of<MatchProvider>(context, listen: false).addLetter(letter!);
          }
        }

      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: color,
        ),
        alignment: Alignment.center,
        child: Text(letter != null ? letter!.letter : '', style: TextStyle(fontSize: fontSize, color: Colors.blue))
      ),
    );
  }
}
