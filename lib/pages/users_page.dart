import 'package:flutter/material.dart';
import 'package:words/components/drawer.dart';
import 'package:words/models/user.dart';
import 'package:words/repositories/users_repository.dart';
import 'package:words/services/location_manager.dart';

class UsersPage extends StatefulWidget {
  const UsersPage({Key? key}) : super(key: key);

  @override
  State<UsersPage> createState() => _UsersPageState();
}

class _UsersPageState extends State<UsersPage> {

  bool loaded = false;
  bool hasError = false;
  List<User> users= [];

  @override
  void initState() {
    LocationManager().updatePosition();
    getData();
    //super.initState();
  }

  Future<void> getData() async {

    try{
      users = await UsersRepository().getUsers();
      print(users);
    }catch(error){
      hasError = true;
      print('has error when loading users: $error');
    }finally{
      setState(() {
        loaded = true;
      });
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: DrawerWords(),
      appBar: AppBar(title: Text('Users page')),
      body: loaded ? Container(
        color: Colors.white,
        child: !hasError ? Text('Error on loading data form API')
            : RefreshIndicator(
              onRefresh: getData,
              child: ListView.builder(
                itemCount: users.length,
                itemBuilder: (context, index){
                  return ListTile(
                    title: Text('${users[index].email}'),
                    subtitle: users[index].position != null ?
                      Text('${users[index].position!.lat} / ${users[index].position!.lng}')
                        : Text('no location')
                  );
              }),
            ),
      ) : Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}

