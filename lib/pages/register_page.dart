import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:words/helpers/validators.dart';
import 'package:words/pages/login_page.dart';
import 'package:words/pages/users_page.dart';
import 'package:words/repositories/auth_repository.dart';

class RegisterPage extends StatefulWidget {
  RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  TextEditingController emailController = TextEditingController(text: 'test@test.com');

  TextEditingController passwordController = TextEditingController(text: '123456');

  TextEditingController confirmPasswordController = TextEditingController(text: '123456');

  final _formKey = GlobalKey<FormState>();
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          child: SingleChildScrollView(
            child: Container(
            height: MediaQuery.of(context).size.height,
              alignment: Alignment.center,
              color: Colors.blueGrey,
              child: Container(
                constraints: BoxConstraints(maxWidth: 300),
                child: Form(
                  key:_formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('WORDS', style: TextStyle(fontSize: 80, fontFamily: 'Outfit')),
                      TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        controller: emailController,
                        decoration: InputDecoration(
                            hintText: 'Email'
                        ),
                        validator: (text){
                          text ??= '';

                          return Validators.isEmail(text) ? null : 'Invalid e-mail format';

                        },
                      ),
                      TextFormField(
                        controller: confirmPasswordController,
                        obscureText: true,
                        decoration: InputDecoration(
                            hintText: 'Confirm password'
                        ),
                        validator: (text){
                          text ??= '';

                          return text == passwordController.text ? null : 'Confirm password and password do not match';
                        },
                      ),
                      TextFormField(
                        controller: passwordController,
                        obscureText: true,
                        decoration: InputDecoration(
                            hintText: 'Password'
                        ),
                        validator: (text){
                          text ??= '';

                          return text.length >= 6 ? null : 'Password too short';
                        },
                      ),
                      SizedBox(height: 20),
                      if(isLoading)
                      CircularProgressIndicator(),
                      if(!isLoading)
                      Container(
                        width: double.infinity,
                        child: ElevatedButton(
                          onPressed: () async {
                            if(_formKey.currentState!.validate()){
                              print('FORM IS VALID');

                              var email = emailController.text;
                              var password = passwordController.text;

                              setState(() {
                                isLoading = true;
                              });

                              Map<String, dynamic> response = await AuthRepository().signUp(email, password, password);

                              setState(() {
                                isLoading = false;
                              });

                              if(response.containsKey('token')){
                                Navigator.pushAndRemoveUntil(
                                    context,
                                    MaterialPageRoute(builder: (_) => UsersPage()),
                                        (Route<dynamic> route) => false
                                );
                              }else{
                                ScaffoldMessenger.of(context)
                                    .showSnackBar(
                                    SnackBar(
                                      content: Text('open an acount!!'),
                                      //action: ,
                                    )
                                );

                              }
                            }else{
                              print('FORM NOT VALID');
                            }

                          },
                          child: Text('Sign Up')),
                      ),
                      SizedBox(height: 30),
                      TextButton(
                          onPressed: ()=>{
                            Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(builder: (_) => LoginPage()),
                                (Route<dynamic> route) => false
                            )
                          },
                          child: Text('Have an account? Log in now', style: TextStyle(color: Colors.white))
                      )
                    ],
                  ),
                ),
              )
            ),
          ),
        )
    );
  }
}
