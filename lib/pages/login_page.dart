import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:words/helpers/validators.dart';
import 'package:words/pages/register_page.dart';
import 'package:words/repositories/auth_repository.dart';

import 'game_page.dart';

class LoginPage extends StatelessWidget {
  LoginPage({Key? key}) : super(key: key);

  TextEditingController emailController = TextEditingController(text: 'pau@santandreu.net');
  TextEditingController passwordController = TextEditingController(text: '123456');
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          alignment: Alignment.center,
            color: Colors.blueGrey,
            child: Container(
              constraints: BoxConstraints(maxWidth: 300),
              child: Form(
                key:_formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('WORDS', style: TextStyle(fontSize: 80, fontFamily: 'Outfit')),
                    TextFormField(
                      keyboardType: TextInputType.emailAddress,
                      controller: emailController,
                      decoration: InputDecoration(
                          hintText: 'Email'
                      ),
                      validator: (text){
                        text ??= '';

                        return Validators.isEmail(text) ? null : 'Invalid e-mail format';

                      },
                    ),
                    TextFormField(
                      controller: passwordController,
                      obscureText: true,
                      decoration: InputDecoration(
                          hintText: 'Password'
                      ),
                      validator: (text){
                        text ??= '';

                        return text.length >= 6 ? null : 'Password too short';
                      },
                    ),
                    SizedBox(height: 20),
                    Container(
                      width: double.infinity,
                      child: ElevatedButton(
                        onPressed: () async {
                          if(_formKey.currentState!.validate()){
                            print('FORM IS VALID');

                            var email = emailController.text;
                            var password = passwordController.text;

                            Map<String, dynamic> response = await AuthRepository().signIn(password, email);

                            if(response.containsKey('token')){
                              Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute(builder: (_) => GamePage()),
                                      (Route<dynamic> route) => false
                              );
                            }else{
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(
                                    SnackBar(
                                      content: Text('open an acount!!'),
                                      action: SnackBarAction(
                                        label: 'OK',
                                        onPressed: (){}
                                      ),
                                    )
                              );

                            }
                          }else{
                            print('FORM NOT VALID');
                          }

                        },
                        child: Text('Log In')),
                    ),
                    SizedBox(height: 30),
                    TextButton(
                      onPressed: ()=>{
                        Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(builder: (_) => RegisterPage()),
                          (Route<dynamic> route) => false
                        )
                      },
                      child: Text('Open a new account', style: TextStyle(color: Colors.white))
                    )
                  ],
                ),
              ),
            )
        ),
      )
    );
  }
}
