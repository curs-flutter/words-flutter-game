import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesManager{

  Future<String?> getToken() async {
    var prefs = await SharedPreferences.getInstance();

    return prefs.getString('token');
  }

  void setToken(String token) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('token', token);
  }

  Future<void> clearData() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }
}