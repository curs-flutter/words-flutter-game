import 'dart:convert';

User userFromJson(String str) => User.fromJson(json.decode(str));
String userToJson(User data) => json.encode(data.toJson());

class User {

  String email;
  DateTime createdAt;
  String userId;
  Position? position;

  User({
    required this.email,
    required this.createdAt,
    required this.userId,
    this.position
  });

  factory User.fromJson(Map<String, dynamic> json)=> User(
    email: json['email'],
    createdAt: DateTime.parse(json['createdAt']),
    userId: json['userId'],
    position: json['position'] != null ? Position.fromJson(json['position']) : null,
  );

  factory User.fromString(userString){
    var json = jsonDecode(userString);

    return User.fromJson(json);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data['position'] = position != null ? position?.toJson() : null;
    data['email'] = email;
    data['createdAt'] = createdAt;
    data['userId'] = userId;

    return data;
  }

  @override
  String toString() => jsonEncode(this);
}

class Position {
  String lng;
  String lat;

  Position({required this.lng, required this.lat});

  factory Position.fromJson(Map<String, dynamic> json)=> Position(
    lng: json['lng'],
    lat: json['lat'],
  );

  Map<String, dynamic> toJson() => {
    "lng": lng,
    "lat": lat,
  };

}