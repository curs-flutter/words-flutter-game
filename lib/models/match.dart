class MatchModel{

  final String id;
  List<String> letters;
  List<Word> words;

  MatchModel({required this.id, required this.letters, required this.words});

  factory MatchModel.formJson(json)=> MatchModel(
    id: json['id'],
    letters: List.from(json['letters']),
    words: List.from(json['words'].map((e)=>Word.fromJson(e)))
  );
}

class Word{
  final String word;
  final int points;

  Word({
    required this.word,
    required this.points
  });

  factory Word.fromJson(json)=> Word(
    word: json['word'],
    points: json['points']
  );
}