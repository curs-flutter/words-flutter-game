class Letter{
  final String letter;
  final int position;
  bool used = false;

  Letter({required this.letter, required this.position});
}