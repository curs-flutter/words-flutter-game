import 'dart:convert';

import "package:http/http.dart" as http;
import 'package:http/http.dart';
import 'package:words/globals.dart';
import 'package:words/models/match.dart';
import 'package:words/models/user.dart';
import 'package:words/services/shared_preferences_manager.dart';

class MatchRepository{

  Future<MatchModel> postCreateMatch() async {

    var token = await SharedPreferencesManager().getToken() ?? "";
    var headers = {"Authorization":token,};

    Uri uri = Uri.https(Globals.apiHost, 'app/api/game/createMatch');

    return http.post(uri, headers: headers).then((http.Response response){

      if(response.statusCode >= 200 && response.statusCode <300) {

        Map<String, dynamic> decodedBody = jsonDecode(response.body);
        MatchModel match = MatchModel.formJson(decodedBody);

        return match;
      }else{
        throw Exception('Error when create match via API: ${response.body}');
      }
    }).catchError((e){
      print('Error connection create match via API: $e');
      throw Exception(e);
    }).whenComplete(() =>  print('Match created via API'));
  }

  Future<MatchModel> startMatch(String matchId) async {

    var token = await SharedPreferencesManager().getToken() ?? "";
    var headers = {"Authorization":token};

    Uri uri = Uri.https(Globals.apiHost, 'app/api/game/start/$matchId');

    return http.post(uri, headers: headers).then((http.Response response){

      if(response.statusCode >= 200 && response.statusCode <300) {

        Map<String, dynamic> decodedBody = jsonDecode(response.body);
        MatchModel match = MatchModel.formJson(decodedBody);

        return match;
      }else{
        throw Exception('Error when start match via API: ${response.body}');
      }
    }).catchError((e){
      print('Error connection start match via API: $e');
      throw Exception(e);
    }).whenComplete(() =>  print('Match started via API'));


  }

}