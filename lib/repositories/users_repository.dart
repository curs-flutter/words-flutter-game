import 'dart:convert';

import "package:http/http.dart" as http;
import 'package:http/http.dart';
import 'package:words/globals.dart';
import 'package:words/models/user.dart';
import 'package:words/services/shared_preferences_manager.dart';

class UsersRepository{

  Future<List<User>> getUsers() async {

    var token = await SharedPreferencesManager().getToken() ?? "";
    var headers = {"Authorization":token,};

    Uri uri = Uri.https(Globals.apiHost, 'app/api/users');

    return http.get(uri, headers: headers).then((http.Response response){

      if(response.statusCode >= 200 && response.statusCode <300) {

        var body = jsonDecode(response.body);
        List<Map<String, dynamic>> decodedResponse = List.from(body);
        List<User> users = decodedResponse.map((e) => User.fromJson(e)).toList();

        return users;
      }else{
        throw Exception('Error when get users via API: ${response.body}');
      }
    }).catchError((e){
      print('Error connection get users via API: $e');
      throw Exception(e);
    }).whenComplete(() =>  print('Users obtained via API'));
  }

  Future<Map<String,dynamic>> updatePosition(double lat, double lng) async {

    var body = {
      "lat":lat.toString(),
      "lng":lng.toString()
    };
    var token = await SharedPreferencesManager().getToken() ?? "";
    var headers = {"Authorization":token};

    Uri uri = Uri.https(Globals.apiHost, 'app/api/aupdatePosition');

    Response response = await http.put(uri,body: body,headers: headers);

    var decodedResponse = jsonDecode(response.body);
    print("DECODED RESPONSE ${decodedResponse}");

    if(response.statusCode < 300){

      return {"message":decodedResponse['message']};
    }else{

      return {"error":decodedResponse['error']};
    }
  }

}