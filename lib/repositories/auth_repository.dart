import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:words/globals.dart';
import 'package:words/services/shared_preferences_manager.dart';

class AuthRepository{

  Future<Map<String, dynamic>> signUp(String email, String pass, String confPass) async {

    Map<String, String> parameters = {
      'email': email,
      'password': pass,
      'confirmPassword': confPass
    };

    Uri uri = Uri.https(Globals.apiHost, 'app/api/auth/signup');
    var body = {
      'email': email,
      'password': pass,
      'confirmPassword': confPass
    };
    var headers = {
      'Content-type': 'application/json',
    };

    var response = await http.post(uri, body: body);

    if (response.statusCode >= 200 && response.statusCode <300) {

      var decodedResponse = jsonDecode(response.body);
      SharedPreferencesManager().setToken(decodedResponse['token']);
      print('Token: ${decodedResponse['token']}');

      return {'token': decodedResponse['token']};
    } else {
      print('response: ${response.body}');
      return {'error': response.body};
    }
  }

  Future<Map<String, dynamic>> signIn(String pass, String email) async {

    Map<String, String> parameters = {
      'email': email,
      'password': pass,
    };

    Uri uri = Uri.https(Globals.apiHost, 'app/api/auth/signin');

    var body = {
    'email': email,
    'password': pass,
    };
    var headers = {
      'Content-type': 'application/json'
    };
    print({uri});
    var response = await http.post(uri, body: body);

    if (response.statusCode >= 200 && response.statusCode <300) {

      var decodedResponse = jsonDecode(response.body);
      SharedPreferencesManager().setToken(decodedResponse['token']);
      print('Token: ${response}');

      return {'token': decodedResponse['token']};
    } else {
      print('response: ${response.body}');
      return {'error': response.body};
    }
  }

}